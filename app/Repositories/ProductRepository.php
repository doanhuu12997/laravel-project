<?php
namespace App\Repositories;

use App\Models\Role;

use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository
{

    public function model()
    {
        return Product::class;
    }

    /**getWithPaginate
     * @param $limit
     * @return mixed
     */
    public function getWithPaginate($limit = 5): mixed
    {
        return $this->model->latest('id')->paginate($limit);
    }

}
